using Xunit;

namespace MaterialDesign.Icons.Tests
{
	public class IconTypeTests
	{
		private readonly MaterialIconType iconTypeBasic,
										  iconTypeStartsWithNumber,
										  iconTypeIsReservedWord;

		public IconTypeTests()
		{
			this.iconTypeBasic = new MaterialIconType( "basic_icon" );
			this.iconTypeStartsWithNumber = new MaterialIconType( "3d_icon" );
			this.iconTypeIsReservedWord = new MaterialIconType( "class" );
		}

		[ Fact ]
		public void TestCamelCaseUpper()
		{
			Assert.Equal( "BasicIcon", this.iconTypeBasic.CamelCaseNameUpper );
			Assert.Equal( "3dIcon", this.iconTypeStartsWithNumber.CamelCaseNameUpper );
			Assert.Equal( "Class", this.iconTypeIsReservedWord.CamelCaseNameUpper );
		}

		[ Fact ]
		public void TestCamelCaseLower()
		{
			Assert.Equal( "basicIcon", this.iconTypeBasic.CamelCaseNameLower );
			Assert.Equal( "3dIcon", this.iconTypeStartsWithNumber.CamelCaseNameLower );
			Assert.Equal( "class", this.iconTypeIsReservedWord.CamelCaseNameLower );
		}

		[ Fact ]
		public void TestIdentifierUpper()
		{
			Assert.Equal( "BasicIcon", this.iconTypeBasic.IdentifierUpper );
			Assert.Equal( "@3dIcon", this.iconTypeStartsWithNumber.IdentifierUpper );
			Assert.Equal( "Class", this.iconTypeIsReservedWord.IdentifierUpper );
		}

		[ Fact ]
		public void TestIdentifierLower()
		{
			Assert.Equal( "basicIcon", this.iconTypeBasic.IdentifierLower );
			Assert.Equal( "@3dIcon", this.iconTypeStartsWithNumber.IdentifierLower );
			Assert.Equal( "@class", this.iconTypeIsReservedWord.IdentifierLower );
		}
	}
}