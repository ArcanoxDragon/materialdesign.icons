/// <binding BeforeBuild='default' />
// ReSharper disable ReplaceIndicingWithShortHandPropertiesAfterDestructuring

var fs = require( "fs" );
var gulp = require( "gulp" );
var request = require( "request" );

const StringBuilder = require( "stringbuilder" );

const keywords = [
	"class",
	"enum",
	"interface",
	"readonly",
	"public",
	"private",
	"protected",
	"int",
	"double",
	"float",
	"string"
];

function isKeyword( str ) {
	return keywords.includes( str );
}

function isValidIdentifier( str ) {
	return !isKeyword( str ) && !str.match( /^\d/ );
}

function writeEnum( done ) {
	request( "https://raw.githubusercontent.com/google/material-design-icons/master/iconfont/codepoints", (err, res, body) => {
		if ( err ) {
			console.log( err );
			return;
		}

		var lines = body.split( /[\r\n]+/ );
		var codepoints = lines.map( l => {
			var match = l.match( /^([a-z0-9_]+) ([a-f0-9]{4})$/i );

			if ( !match ) return null;

			return {
				name: match[ 1 ],
				code: match[ 2 ],
				enumName: match[ 1 ].replace( /(^|_)([a-z0-9])/g, (m, g1, g2) => g2.toUpperCase() )
			};
		} ).filter( c => c !== null );

		var contents = new StringBuilder();

		contents
			.appendLine( "namespace MaterialDesign.Icons {" )
			.appendLine( "public enum MaterialIcon {" )
			.appendLine( "Unknown = -1," );

		codepoints.forEach( c => {
			var identifier = isValidIdentifier( c.enumName ) ? c.enumName : `Icon${ c.enumName }`;
			contents.appendLine( `[IconFontName("${ c.name }")] ${ identifier } = 0x${ c.code },` );
		} );

		contents.appendLine( "}}" );

		var file = fs.createWriteStream( "MaterialIcon.cs" );
		contents.pipe( file );
		contents.flush();

		done();
	} );
}

gulp.task( "default", writeEnum );