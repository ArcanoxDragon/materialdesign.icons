﻿using System;

namespace MaterialDesign.Icons
{
	class IconFontNameAttribute : Attribute
	{
		public IconFontNameAttribute( string iconFontName )
		{
			this.Name = iconFontName;
		}

		public string Name { get; }
	}
}