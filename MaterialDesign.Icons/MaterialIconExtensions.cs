﻿using System.Linq;
using System.Reflection;

namespace MaterialDesign.Icons
{
	public static class MaterialIconExtensions
	{
		public static string ToIconFontString( this MaterialIcon icon )
		{
			var type = icon.GetType();
			var members = type.GetFields( BindingFlags.DeclaredOnly | BindingFlags.Static | BindingFlags.Public );
			var member = members.FirstOrDefault( fi => fi.GetValue( null ) is MaterialIcon i && i == icon );
			var attribute = member?.GetCustomAttribute<IconFontNameAttribute>();

			return attribute?.Name;
		}
	}
}